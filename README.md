# Kahoot 2

## Building and running
You need Qt creator to build this project. It can be downloaded [here](https://www.qt.io/download-qt-installer).  
Open the project and click "build" (the hammer icon on the lower left).  
To run, click the top green arrow.

## Background  
The purpose of this project was to become familiar with Qt designer, as well as  
connecting different GUI components together with slots.


## Playing  
When starting the application, you will be asked for your username.  
After entering it and pressing "start", you will be asked a series of questions, and  
given 4 options for each. Press the answer you think is correct.  
You will see a live score as you go, and will be presented with a "results" screen at the end.
