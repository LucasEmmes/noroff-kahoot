#ifndef KAHOOTQUESTION_H
#define KAHOOTQUESTION_H

#include <string>
#include <QString>

class KahootQuestion
{
private:
    QString question;
    std::vector<QString> options;
    int correct_answer;

public:
    KahootQuestion();
    KahootQuestion(std::string question, std::vector<std::string> options, int answer);

    // Getters
    auto get_question() -> QString;
    auto get_options() -> std::vector<QString>;
    bool check_guess(int guess);
};

#endif // KAHOOTQUESTION_H
