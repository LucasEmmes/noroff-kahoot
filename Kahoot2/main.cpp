#include "kahoot.h"
#include "kahootquestion.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Kahoot w;
    w.setWindowTitle("Kahoot 2");
    w.show();
    return a.exec();
}
