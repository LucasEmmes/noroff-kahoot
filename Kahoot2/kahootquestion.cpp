#include "kahootquestion.h"

KahootQuestion::KahootQuestion()
{

}

KahootQuestion::KahootQuestion(std::string question, std::vector<std::string> options, int answer)
{
    this->question = QString::fromStdString(question);
    this->correct_answer = answer;
    for (auto& it : options)
        this->options.push_back(QString::fromStdString(it));
}

QString KahootQuestion::get_question() {
    return this->question;
}

std::vector<QString> KahootQuestion::get_options() {
    return this->options;
}

bool KahootQuestion::check_guess(int guess) {
    return this->correct_answer == guess;
}
