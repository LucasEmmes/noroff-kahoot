#include "kahoot.h"
#include "ui_kahoot.h"
#include "kahootquestion.h"
#include <QFont>
#include <QPropertyAnimation>
#include <qvariant.h>

QVariant myFontInterpolator(const QFont &start, const QFont &end, qreal progress)
{
    int a = start.pointSize();
    int b = end.pointSize();
    int c = (1-progress)*a + progress*b;
    QFont rt("Segoe UI", c);
    return (rt);
}

Kahoot::Kahoot(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Kahoot)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentWidget(ui->PAGE_START);
    qRegisterAnimationInterpolator<QFont>(myFontInterpolator);
}

Kahoot::~Kahoot()
{
    delete ui;
    delete result_anim;
}

void Kahoot::add_score(int points_earned) {
    this->score += points_earned;
}

void Kahoot::restart_game() {
    this->score = 0;
    this->current_question = -1;
    ui->LINEEDIT_STAR_USERNAME->setText(QString::fromStdString(""));
    ui->stackedWidget->setCurrentWidget(ui->PAGE_START);
}

KahootQuestion Kahoot::get_current_question() {
    return this->questions[this->current_question];
}

void Kahoot::load_results() {
    // Set username on results screen
    ui->LABEL_RESULTS_USERNAME->setText(this->username);

    // Set label announcing number of correct answers
    QString label_number_of_correct = QString::number((int) this->score/1000) + QString::fromStdString(" out of ")
            + QString::number(this->questions.size()) + QString::fromStdString(" correct!");
    ui->LABEL_RESULTS_CORRECT->setText(label_number_of_correct);

    // Set label announcing final score
    QString label_final_score = QString::fromStdString("Final score: ") + QString::number(this->score) + QString::fromStdString(" points!");
    ui->LABEL_RESULTS_SCORE->setText(label_final_score);

    ui->stackedWidget->setCurrentWidget(ui->PAGE_RESULTS);

    result_anim = new QPropertyAnimation(ui->LABEL_STATIC_RESULTS_ANNOUNCE, "font");
    result_anim->setStartValue(QFont("Segoe UI", 5));
    result_anim->setEndValue(QFont("Segoe UI", 30));
    result_anim->setDuration(700);
    result_anim->start();
}

void Kahoot::load_evaluation(bool temp_result) {
    QString points_earned;
    // Set label announcing answer on last question
    if (temp_result) {
        ui->LABEL_EVAL_ANSWER_STATUS->setText("Correct!");
        points_earned = QString::fromStdString("1000");
        this->add_score(1000);
    } else {
        ui->LABEL_EVAL_ANSWER_STATUS->setText("Incorrect!");
        points_earned = QString::fromStdString("0");
    }

    ui->LABEL_EVAL_POINTS->setText(QString::fromStdString("You gained ") + points_earned + QString::fromStdString(" points!"));
    ui->stackedWidget->setCurrentWidget(ui->PAGE_EVAL_ANSWER);
}

void Kahoot::load_next_question() {
    this->current_question++;

    // Set question textbrowser
    ui->TEXTBROWSER_QUIZ_QUESTION->setText(get_current_question().get_question());

    // Set options on buttons
    ui->BUTTON_QUIZ_A->setText(get_current_question().get_options()[0]);
    ui->BUTTON_QUIZ_B->setText(get_current_question().get_options()[1]);
    ui->BUTTON_QUIZ_C->setText(get_current_question().get_options()[2]);
    ui->BUTTON_QUIZ_D->setText(get_current_question().get_options()[3]);

    // Set username and score labels
    QString label_quiz_userscore = this->username + QString::fromStdString(" : ") + QString::number(this->score);
    ui->LABEL_QUIZ_USERSCORE->setText(label_quiz_userscore);

    ui->stackedWidget->setCurrentWidget(ui->PAGE_QUIZ);
}

void Kahoot::advance_screen() {
    if (this->current_question < (int) this->questions.size()-1)
        load_next_question();
    else
        load_results();
}

void Kahoot::make_guess(int guess) {
    load_evaluation(get_current_question().check_guess(guess));
}

void Kahoot::on_BUTTON_START_START_clicked()
{
    if (!ui->LINEEDIT_STAR_USERNAME->text().isEmpty()) {
        this->username = ui->LINEEDIT_STAR_USERNAME->text();
        advance_screen();
    }
}


void Kahoot::on_BUTTON_QUIZ_A_clicked()
{
    this->make_guess(0);
}


void Kahoot::on_BUTTON_QUIZ_B_clicked()
{
    this->make_guess(1);
}


void Kahoot::on_BUTTON_QUIZ_C_clicked()
{
    this->make_guess(2);
}


void Kahoot::on_BUTTON_QUIZ_D_clicked()
{
    this->make_guess(3);
}




void Kahoot::on_BUTTON_EVAL_OK_clicked()
{
    this->advance_screen();
}


void Kahoot::on_BUTTON_RESULTS_RESTART_clicked()
{
    this->restart_game();
}

