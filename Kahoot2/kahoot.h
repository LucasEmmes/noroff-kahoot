#ifndef KAHOOT_H
#define KAHOOT_H

#include <QMainWindow>
#include <qpropertyanimation.h>
#include <vector>
#include "kahootquestion.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Kahoot; }
QT_END_NAMESPACE

class Kahoot : public QMainWindow
{
    Q_OBJECT

public:
    Kahoot(QWidget *parent = nullptr);
    ~Kahoot();

    // Gameflow
    void add_score(int points_earned);
    void restart_game();

    KahootQuestion get_current_question();

    void advance_screen();

    void load_results();
    void load_evaluation(bool temp_result);
    void load_next_question();

    void make_guess(int guess);

private slots:
    void on_BUTTON_START_START_clicked();

    void on_BUTTON_QUIZ_A_clicked();

    void on_BUTTON_QUIZ_B_clicked();

    void on_BUTTON_QUIZ_D_clicked();

    void on_BUTTON_QUIZ_C_clicked();

    void on_BUTTON_EVAL_OK_clicked();

    void on_BUTTON_RESULTS_RESTART_clicked();

private:
    Ui::Kahoot *ui;

    QString username;
    int score = 0;
    QPropertyAnimation* result_anim;

    int current_question = -1;
    std::vector<KahootQuestion> questions = {
        KahootQuestion("How many tennisballs are used at Wimbleton every year?", {"55'000", "12'000", "8'000", "30'000"}, 0),
        KahootQuestion("What is the earth's diameter?", {"5'700 m", "10'500 m", "12'700 m", "20'100 m"}, 2),
        KahootQuestion("How long is the world's largest cruise ship?", {"256 m", "362 m", "560 m", "420 m"}, 1),
        KahootQuestion("How tall was Robert Wadlow?", {"269 cm", "272 cm", "190 cm", "302 cm"}, 1),
        KahootQuestion("Am I currently broke?", {"Perhaps", "Maybe", "No", "Yes"}, 3),
    };
};
#endif // KAHOOT_H
